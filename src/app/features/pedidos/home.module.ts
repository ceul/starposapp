import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { homeRouting } from './home.routing';
import {HomeComponent} from "./home.component";
import { SharedModule } from '@app/shared/shared.module';

// Components
import { AppComponent } from '../../app.component'; // ?
import { Routes } from "@angular/router";
import { UsersComponent } from "@app/features/pedidos/users/users.component";
import { UserService } from "@app/core/services/services-octopos/user.service";
import { CategoriesComponent } from "@app/features/pedidos/categories/categories.component";
import { CategoriesService } from "@app/core/services/services-octopos/categories.service";
import { ProductsService } from "@app/core/services/services-octopos/products.service";
import { ProductsComponent } from "@app/features/pedidos/products/products.component";
import { PlacesService } from "@app/core/services/services-octopos/places.service";
import { PlacesComponent } from "@app/features/pedidos/places/places.component";
import { FloorsService } from "@app/core/services/services-octopos/floors.service";
import { SharedTicketsService } from "@app/core/services/services-octopos/sharedtickets.service";
import { GlobalsService } from "@app/core/services/services-octopos/global.service";
import { TaxesService } from "@app/core/services/services-octopos/taxes.service";
import { OrdersComponent } from "@app/features/pedidos/orders/orders.component";
import { NotificationService } from "@app/core/services";
import { AuthenticationService } from "@app/core/services/services-octopos/auth.service";

@NgModule({
  imports: [
    CommonModule,
    homeRouting,
    SharedModule
  ],
  declarations: [
    HomeComponent,
    CategoriesComponent,
    ProductsComponent,
    UsersComponent,
    PlacesComponent,
    OrdersComponent
  ],providers: [
		UserService,
    CategoriesService,
    ProductsService,
    PlacesService,
    FloorsService,
    SharedTicketsService,
    GlobalsService,
    TaxesService,
    NotificationService,
    AuthenticationService
	],
})
export class HomeModule { }
