import { Component, OnInit } from '@angular/core';
import { HeroesService, Heroe } from '../../../core/services/services-octopos/heroes.service';
import { Router } from '@angular/router';
import { UserService } from "@app/core/services/services-octopos/user.service";
import { GlobalsService } from "@app/core/services/services-octopos/global.service";
import { NotificationService } from "../../../shared/utils/notification.service";
import { AuthenticationService } from "@app/core/services/services-octopos/auth.service";
import { ViewChild } from "@angular/core";
import { ModalDirective } from "ngx-bootstrap";
@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styles: []
})
export class UsersComponent implements OnInit {
  users: any[];
  public password1: string;
  id: string;
  user: string;
  error: boolean;

  constructor(private userService: UserService,
    private authService: AuthenticationService,
    private _router: Router,
    private globalService: GlobalsService,
    private notificationService: NotificationService
  ) {
  }
  @ViewChild('passModal') public passModal:ModalDirective;
  
  ngOnInit() {
    try {
      this.password1 = "";
      this.error = false;
      let servicio = this.userService.obtenerUsuarios().subscribe(
        resp => {
          this.users = resp;
          console.log(this.users);
          servicio.unsubscribe();
        },
        errResponse => {
          servicio.unsubscribe();
          throw new Error(errResponse);
        }
      );
    }
    catch (error) {
      console.log(error);
    }
  }

  goToPlaces(id: string, user: string) {
    try {
      debugger
      this.globalService.globals.user = user;
      this.globalService.globals.id = id;
      this._router.navigate(['/order/places']);
    }
    catch (error) {
      console.log(error);
    }
  }

  showModal(id: string, user: string) {
    this.password1 = "";
    this.error = false;
    this.passModal.show();
    this.user = user;
    this.id = id;
    /*let message: string;
    if(flag==0){
      message = "Please enter your password1";
    }else {
      message = "Wrong password1, please enter your password1";
    }
    this.notificationService.smartMessageBox({
      title: "Login form",
      content: message,
      buttons: "[Cancel][Login]",
      input: "text",
      placeholder: "Enter your password1"
    }, (ButtonPress, Value) => {
      if (ButtonPress == "Cancel") {
        return 0;
      } else {
        this.login(id,user,Value);
      }
    });*/

  }

  login() {
    debugger
    let servicio = this.authService.login(this.user, this.password1).subscribe(
      resp => {
        debugger;
        this.error = false;
        this.passModal.hide();
        this.goToPlaces(this.id, this.user);
        servicio.unsubscribe();
      },
      errResponse => {
        //this.showDialog(id, user, 1);
        this.error = true;
        debugger;
        servicio.unsubscribe();
        throw new Error(errResponse);
      }
    );
  }

}
