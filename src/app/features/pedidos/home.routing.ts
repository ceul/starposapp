import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from "./home.component";
import {ModuleWithProviders} from "@angular/core";
import { UsersComponent } from "./users/users.component";
import { CategoriesComponent } from "./categories/categories.component";
import { ProductsComponent } from "@app/features/pedidos/products/products.component";
import { PlacesComponent } from "@app/features/pedidos/places/places.component";
import { OrdersComponent } from "@app/features/pedidos/orders/orders.component";
import { AuthGuard } from "../../core/guards/auth.guard"
export const homeRoutes: Routes = [
  /*  {
        path: '',
        component: HomeComponent,
        data: {
            pageTitle: 'Home'
        }
    },*/
    {
        path: '',
        component: UsersComponent,
        data: {
            pageTitle: 'Users'
        }
    },
    {
        path: 'places',
        component: PlacesComponent,
        canActivate: [AuthGuard],
        data: {
            pageTitle: 'Places'
        }
    },
    {
        path: 'orders/:id',
        component: OrdersComponent,
        data: {
            pageTitle: 'Order'
        }
    },
    {
        path: 'categories',
        component: CategoriesComponent,
        data: {
            pageTitle: 'Categories'
        }
    },
    {
        path: 'products/:id',
        component: ProductsComponent,
        data: {
            pageTitle: 'Products'
        }
    }
];
export const homeRouting: ModuleWithProviders = RouterModule.forChild(homeRoutes);