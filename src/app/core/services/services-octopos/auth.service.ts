import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import BaseService from "@app/core/services/services-octopos/base-service";

@Injectable({ providedIn: 'root' })
export class AuthenticationService extends BaseService {

	constructor(public http: HttpClient) {
		super(http);
	}
	
	login(username: string, password: string) {
		try{
			return this.consumeService('/authenticate',{ username, password })
            .pipe(map(user => {
                // login successful if there's a jwt token in the response
                if (user && user['token']) {
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify(user));
                }

                return user;
            }));
		}
		catch  (error) {
			console.log(error);
		}
	}

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
    }

}