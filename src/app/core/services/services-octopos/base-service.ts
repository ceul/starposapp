import { HttpClient } from '@angular/common/http';
import { Observable, Observer } from "rxjs";

export default class BaseService {
	defaultHeaders: {} = {
		'Content-type': 'application/json'
	};
	constructor(public http: HttpClient) {

	}

	/**
	 * Consume de un servicio del api y devuelve un observable
	 * @param url url del servicio del api
	 * @param data los parámetros que van en el body de la petición
	 * @returns {observable}
	 */
	consumeService(url: string, data?: any): Observable<any> {
		return this.http.post("http://localhost:3000" + url, data);
	}
}