import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

declare var $: any;

@Injectable()
export class GlobalsService {
	globals: {
		id: string,
		user: string,
		sharedTicked: any,
		isNew: boolean
	};
	private static _instance: GlobalsService;

	constructor() {
		this.globals = {
			"id": "",
			"user": "",
			"sharedTicked": null,
			"isNew": true
		};
		GlobalsService._instance = this;
	}

	public static get Instance(): GlobalsService {
		return this._instance;
	}


	getGlobals() {
		try {
			debugger
			return this.globals;
		} catch (error) {
			console.log(error);
		}
	}

	setUser(user: string, id: string) {
		try {
			this.globals.user = user;
			this.globals.id = id;
			return this.globals;
		} catch (error) {
			console.log(error);
		}
	}

    /*setPlace( place: string) {
		try {
            this.globals['place'] = place;
			return this.globals;
		} catch (error) {
			console.log(error);
		}
	}*/

	setSharedTicked(sharedTicked: any, isnew: boolean) {
		try {
			this.globals.isNew = isnew;
			this.globals.sharedTicked = sharedTicked;
			return this.globals;
		} catch (error) {
			console.log(error);
		}
	}

	getSharedTicket() {
		try {
			return this.globals.sharedTicked;
		} catch (error) {
			console.log(error);
		}
	}
}
