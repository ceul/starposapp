import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import BaseService from './base-service';
import { Observable } from 'rxjs';

@Injectable()
export class FloorsService extends BaseService {

	constructor(public http: HttpClient) {
		super(http);
	}
	
	getFloors() {
		try{
			return this.consumeService('/floor/get');
		}
		catch  (error) {
			console.log(error);
		}
	}

}