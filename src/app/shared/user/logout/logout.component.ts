import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { NotificationService } from "@app/core/services/notification.service";

import {UserService} from "@app/core/services/user.service";
import { AuthenticationService } from "@app/core/services/services-octopos/auth.service";
import { GlobalsService } from "@app/core/services/services-octopos/global.service";

@Component({
  selector: "sa-logout",
  template: `
<div id="logout" (click)="showPopup()" class="btn-header transparent pull-right">
        <span> <a title="Sign Out"><i class="fa fa-sign-out"></i></a> </span>
    </div>
  `,
  styles: []
})
export class LogoutComponent implements OnInit {

  public user

  constructor(
    private userService: UserService,
    private router: Router,
    private notificationService: NotificationService,
    private authService: AuthenticationService,
    private globalService: GlobalsService
  ) {
  }

  showPopup() {
    debugger
    let user = this.globalService.globals.user;
    this.notificationService.smartMessageBox(
      {
        title:
          "<i class='fa fa-sign-out txt-color-orangeDark'></i> Logout <span class='txt-color-orangeDark'><strong>" + user+"</strong></span> ?",
        content:
          "Are you sure, that you want to logout?",
        buttons: "[No][Yes]"
      },
      ButtonPressed => {
        if (ButtonPressed == "Yes") {
          this.logout();
        }
      }
    );
  }

  logout() {
    this.router.navigate(['/order']);
    this.authService.logout();
    this.userService.logout()
  }

  ngOnInit() {}
}
